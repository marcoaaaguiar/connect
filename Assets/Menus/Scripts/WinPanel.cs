﻿using System;
using System.Collections;
using UnityEngine;

namespace Menus.Scripts {
    public class WinPanel : MonoBehaviour {
        private CanvasGroup _canvasGroup;

        private void Awake() {
            WinCondition.Won += OnWon;
        }

        private void Start() {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnDestroy() {
            WinCondition.Won -= OnWon;
        }

        private void OnWon(object sender, EventArgs e) {
            StartCoroutine(FadeIn(1));
        }

        private IEnumerator FadeIn(float time) {
            var elapsedTime = 0.0f;
            while (elapsedTime <= time) {
                elapsedTime += Time.deltaTime;
                _canvasGroup.alpha = elapsedTime / time;
                yield return null;
            }

            _canvasGroup.alpha = 1;
        }
    }
}