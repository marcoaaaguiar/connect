﻿using System;
using TMPro;
using UnityEngine;

namespace Menus.Scripts {
    public class LevelInformationPanel : MonoBehaviour {
        [SerializeField] private TextMeshProUGUI worldNameText;
        [SerializeField] private TextMeshProUGUI levelNumberText;
        [SerializeField] private TextMeshProUGUI totalTime;
    
        private void Start() {
            var currentLevelData =  LevelLoader.Instance.GetCurrentLevelData();

            worldNameText.text = string.Format(worldNameText.text, currentLevelData.board.text);
            levelNumberText.text = string.Format(levelNumberText.text, currentLevelData.levelNumber.ToString());
//        _totalTime.text = string.Format(_totalTime.text, LevelTimer.Instance);

            WinCondition.Won += OnWon;
        }

        private void OnWon(object sender, EventArgs e) {
            Debug.Log("!");
            totalTime.text = string.Format(totalTime.text, LevelTimer.Instance);
        }

        private void OnDestroy() {
            WinCondition.Won -= OnWon;
        }
    }
}
