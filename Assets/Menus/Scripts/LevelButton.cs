﻿using TMPro;
using UnityEngine;

namespace Menus.Scripts {
    public class LevelButton : MonoBehaviour {
        [SerializeField] TextMeshProUGUI textMeshProUgui;
        [SerializeField] private GameObject checkMarkIcon;
        [SerializeField] private GameObject lockedIcon;
        public int levelNumber;
        public string levelHash;
        public bool locked;
        public bool completed;

        public void UpdateView() {
            textMeshProUgui.text = levelNumber.ToString();
            checkMarkIcon.SetActive(completed);
            lockedIcon.SetActive(locked);
        }

        public void PlayLevel() {
            LevelLoader.Instance.SetCurrentLevelHash(levelHash);
            SimpleSceneFader.ChangeSceneWithFade("PlayScene");
        }
    }
}