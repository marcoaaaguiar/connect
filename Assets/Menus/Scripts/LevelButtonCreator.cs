﻿using System.Linq;
using Menus.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonCreator : MonoBehaviour {
    [SerializeField] private GameObject levelButtonPrefab;
    [SerializeField] private GameObject levelPanel;

    public static LevelButtonCreator Instance;

    private void Awake() {
        if (Instance != null) Destroy(gameObject);
        else {
            Instance = this;
        }
    }

    public void ShowButtons(string world) {
        levelPanel.SetActive(true);
        ClearButtons();
        var levelDataList = LevelLoader.Instance.WorldLevelDict[world];
        foreach (var levelData in levelDataList.OrderBy(item => item.levelNumber)) {
            var go = Instantiate(levelButtonPrefab, levelPanel.transform.position, Quaternion.identity, levelPanel.transform);
            var levelButton = go.GetComponent<LevelButton>();
            levelButton.levelNumber = levelData.levelNumber;
            levelButton.levelHash = levelData.uniqueHash;
            levelButton.completed = SaveSystem.Current.completed[levelData.uniqueHash];
            levelButton.locked = SaveSystem.Current.locked[levelData.uniqueHash];

            go.GetComponent<Button>().interactable = !levelButton.locked;
            levelButton.UpdateView();
        }
    }

    private void ClearButtons() {
        foreach (var levelButton in levelPanel.GetComponentsInChildren<LevelButton>()) {
            Destroy(levelButton.gameObject);
        }
    }
}