﻿using UnityEngine;

public class ButtonSceneLoadAction : MonoBehaviour
{
    public void TransitionToScene(string sceneName) {
        SimpleSceneFader.ChangeSceneWithFade(sceneName);
    }
}