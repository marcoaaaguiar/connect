﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WorldButton : MonoBehaviour {
    public TextMeshProUGUI textMeshProUgui;
    public string worldName;

    private void Start() {
        var worldButton = GetComponentInChildren<TextMeshProUGUI>();
        worldButton.text = worldName;
    }

    private void ShowWorldLevels() {
        var worldSelectionPanelGameObject = transform.parent.gameObject;
        worldSelectionPanelGameObject.SetActive(false);
        LevelButtonCreator.Instance.ShowButtons(worldName);
    }
}