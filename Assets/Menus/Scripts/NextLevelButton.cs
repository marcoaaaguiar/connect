﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelButton : MonoBehaviour {
    public string levelHash;

    private void Start() {
        levelHash = LevelLoader.Instance.GetNextHashLevel();
    }

    public void PlayNextLevel() {
        if (levelHash != null) {
            LevelLoader.Instance.SetCurrentLevelHash(levelHash);
            // TODO: use fading
            SceneManager.LoadScene("PlayScene");
        }
        else {
            LevelLoader.Instance.SetCurrentLevelHash(levelHash);
            SimpleSceneFader.ChangeSceneWithFade("LevelSelect");
        }
    }
}