﻿using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WorldButtonCreator : MonoBehaviour {
    [SerializeField] private GameObject worldButtonPrefab;
    [SerializeField] private GameObject panel;

    private void Start() {
        foreach (var world in LevelLoader.Instance.WorldLevelDict.Keys) {
            var go = Instantiate(worldButtonPrefab, panel.transform.position, Quaternion.identity, panel.transform);
            go.GetComponent<WorldButton>().worldName = world;
            var button = go.GetComponent<Button>();
            
        }
    }
}