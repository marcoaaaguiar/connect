﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class LevelData {
    [NonSerialized] private const string LevelFolder = "/Data/Levels/";
    public string uniqueHash;
    public int levelNumber;
    public int groups;
    public List<PieceData> points;
    public BoardData board;

    public static LevelData LoadLevel(int levelId) {
        var fullPath = Application.dataPath + LevelFolder;
        var text = File.ReadAllText(fullPath + "level" + levelId + ".json");
        Debug.Log(text);
        return JsonConvert.DeserializeObject<LevelData>(text);
    }

    public static List<LevelData> LoadAll() {
        var allLevels = new List<LevelData>();
        var fullPath = Application.dataPath + LevelFolder;
        foreach (var file in Directory.GetFiles(fullPath)) {
            if (file.EndsWith(".json")) {
                var text = File.ReadAllText(file);
                allLevels.Add(JsonConvert.DeserializeObject<LevelData>(text));
            }
        }

        return allLevels;
    }
}