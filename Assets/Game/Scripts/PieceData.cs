﻿using System;
using System.Collections.Generic;

[Serializable]
public class PieceData {
    public int group;
    public List<int> position;
    public string type;
}