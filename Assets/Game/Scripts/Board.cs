﻿using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {
    public static Board Instance;

    public BaseTile[,] Tiles { get; set; }
    public List<BasePiece> Pieces { get; set; }
    public List<PieceGroup> Groups { get; set; }

    private void Awake() {
        if (Instance != null) Destroy(gameObject);
        else {
            Instance = this;
        }
    }

    public bool AreNeighbors(BaseTile tile1, BaseTile tile2) {
        var coordinate1 = GetTileCoordinate(tile1);
        if (coordinate1.HasValue) {
            var x = coordinate1.Value.Item1;
            var y = coordinate1.Value.Item2;

            if (x < Tiles.GetLength(0) - 1 && Tiles[x + 1, y] == tile2) {
                return true;
            }

            if (y < Tiles.GetLength(1) - 1 && Tiles[x, y + 1] == tile2) {
                return true;
            }

            if (x > 0 && Tiles[x - 1, y] == tile2) {
                return true;
            }

            if (y > 0 && Tiles[x, y - 1] == tile2) {
                return true;
            }
        }

        return false;
    }

    public (int, int)? GetTileCoordinate(BaseTile tile) {
        for (var xIndex = 0; xIndex < Tiles.GetLength(0); xIndex++) {
            for (var yIndex = 0; yIndex < Tiles.GetLength(1); yIndex++) {
                if (Tiles[xIndex, yIndex] == tile) {
                    return (xIndex, yIndex);
                }
            }
        }

        return null;
    }
}