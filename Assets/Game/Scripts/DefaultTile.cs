﻿public class DefaultTile : BaseTile {
    private void OnMouseDown() {
        PieceLinker.SelectedPiece = piece;
    }

    private void OnMouseUp() {
        PieceLinker.SelectedPiece = null;
    }
    private void OnMouseOver() {
        if (PieceLinker.MouseOverTile != this) {
            PieceLinker.MouseOverTile = this;
        }
    }

    private void OnMouseExit() {
        if (PieceLinker.MouseOverTile == this) {
            PieceLinker.MouseOverTile = null;
        }
    }

    public override bool CanBeFilled => piece == null;
}