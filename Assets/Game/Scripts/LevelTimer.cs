﻿using System;
using UnityEngine;

public class LevelTimer : MonoBehaviour {
    [SerializeField] private float startTime;
    [SerializeField] private float stopTime;
    public bool stopped;

    public static LevelTimer Instance;

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
        }
        else {
            Instance = this;
        }
    }

    private void Start() {
        startTime = Time.time;

        WinCondition.Won += OnWon;
    }
    
    private void OnWon(object sender, EventArgs e) {
        StopTimer();
    }

    private void OnDestroy() {
        WinCondition.Won -= OnWon;
    }

    private float StopTimer() {
        stopped = true;
        stopTime = Time.time;
        return stopTime - startTime;
    }

    private float ElapsedTime() {
        if (stopped) return stopTime - startTime;
        return Time.time - startTime;
    }

    public override string ToString() {
        var elapsed = ElapsedTime();
        return $"{(int) elapsed / 60:00.}:{Mathf.Floor(elapsed % 60):00.}";
    }
}