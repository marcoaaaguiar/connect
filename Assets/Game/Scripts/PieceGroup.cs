﻿using System.Collections.Generic;
using UnityEngine;

public class PieceGroup {
    public Color Color;
    public int GroupId;
    public readonly List<BasePiece> Pieces = new List<BasePiece>();
    public readonly List<PiecePath> Paths = new List<PiecePath>();
}

public class PiecePath {
    public BasePiece Start;
    public BasePiece End;
    public List<BasePiece> Pieces;
}