﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using UnityEngine;

public class LevelLoader : MonoBehaviour {
    public static LevelLoader Instance;
    private string _currentLevelHash;

    public Dictionary<string, LevelData> LevelDict { get; set; }
    public SortedList<string, List<LevelData>> WorldLevelDict { get; private set; }

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        LevelDict = new Dictionary<string, LevelData>();
        WorldLevelDict = new SortedList<string, List<LevelData>>();


        LoadLevelData();

        _currentLevelHash = LevelDict.First().Key;
    }

    private void Start() {
        foreach (var keyValue in LevelDict) {
            if (!SaveSystem.Current.completed.Keys.Contains(keyValue.Key))
                SaveSystem.Current.completed[keyValue.Key] = false;
            if (!SaveSystem.Current.locked.Keys.Contains(keyValue.Key))
                SaveSystem.Current.locked[keyValue.Key] = true;
        }

        if (SaveSystem.Current.locked.Values.All(b => b)) {
            var firstLevelHash = WorldLevelDict.Values.First().First().uniqueHash;
            SaveSystem.Current.locked[firstLevelHash] = false;
        }
        SaveSystem.Save();
    }

    public void MarkLevelAsCompleted() {
        SaveSystem.Current.completed[_currentLevelHash] = true;
    }
    
    public void UnlockNextLevel() {
        SaveSystem.Current.locked[GetNextHashLevel()] = false;
    }

    private void LoadLevelData() {
        foreach (var data in LevelData.LoadAll()) {
            LevelDict[data.uniqueHash] = data;
            var worldName = data.board.text;
            if (!WorldLevelDict.ContainsKey(worldName)) WorldLevelDict[worldName] = new List<LevelData>();
            WorldLevelDict[worldName].Add(data);
        }

        Debug.Log(WorldLevelDict.Count);
        for (var i = 0; i < WorldLevelDict.Count; i++) {
            var worldName = WorldLevelDict.Keys[i];
             WorldLevelDict[worldName] = WorldLevelDict.Values[i].OrderBy(item => item.levelNumber).ToList();
        }
    }

    public void SetCurrentLevelHash(string levelHash) {
        _currentLevelHash = levelHash;
    }
 
    public string GetNextHashLevel() {
        var currentLevelData = LevelDict[_currentLevelHash];
        var currentWorld = currentLevelData.board.text;
        var worldLevelData = WorldLevelDict[currentWorld];
        if (currentLevelData == worldLevelData.Last()) {
            if (currentWorld == WorldLevelDict.Last().Key) {
                return null;
            }

            return WorldLevelDict.Values[WorldLevelDict.IndexOfKey(currentWorld) + 1].First().uniqueHash;
        }

        return worldLevelData[worldLevelData.IndexOf(currentLevelData) + 1].uniqueHash;
    }

    public LevelData GetCurrentLevelData() {
        return LevelDict[_currentLevelHash];
    }
}