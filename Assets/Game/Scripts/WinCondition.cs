﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WinCondition : MonoBehaviour {
    public static WinCondition Instance;
    public static event EventHandler Won;

    private void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
        }
        else {
            Instance = this;
        }
    }

    private void Start() {
        PieceLinker.Connected += OnConnected;
    }

    protected virtual void OnWon() {
        LevelLoader.Instance.MarkLevelAsCompleted();
        LevelLoader.Instance.UnlockNextLevel();

        Won?.Invoke(this, EventArgs.Empty);
        SaveSystem.Save();
    }

    private void OnConnected(object sender, List<BasePiece> e) {
        CheckWinCondition();
    }

    private void CheckWinCondition() {
        var allConnected = Board.Instance.Tiles.Cast<BaseTile>().Aggregate(true, (current, tile) => current && !tile.CanBeFilled);

        if (allConnected && PathsChecker.CountConnectedPaths() == Board.Instance.Groups.Count) {
            Debug.Log("Win!");
            OnWon();
        }
    }
}