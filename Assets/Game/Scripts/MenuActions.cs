﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuActions : MonoBehaviour {
    public void PlayNextLevel() {
        SimpleSceneFader.ChangeSceneWithFade("PlayScene");
    }
    
    public void GoToLevelSelection() {
        SimpleSceneFader.ChangeSceneWithFade("LevelSelect");
    }
}