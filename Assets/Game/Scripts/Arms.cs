﻿using UnityEngine;

public class Arms : MonoBehaviour {
    [SerializeField] private Sprite armRestSprite;
    [SerializeField] private Sprite armReachingSprite;

    private SpriteRenderer _spriteRenderer;

    private void Awake() {
        _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }


    public void SetArmPosition(string positionString) {
        if (positionString == "right") {
            _spriteRenderer.sprite = armReachingSprite;
            SetZRotation(0);
        }
        else if (positionString == "up") {
            _spriteRenderer.sprite = armReachingSprite;
            SetZRotation(270);
        }
        else if (positionString == "left") {
            _spriteRenderer.sprite = armReachingSprite;
            SetZRotation(0);
        }
        else if (positionString == "down") {
            _spriteRenderer.sprite = armReachingSprite;
            SetZRotation(90);
        }
        else if (positionString == "rest") {
            _spriteRenderer.sprite = armRestSprite;
            SetZRotation(0);
        }
    }

    private void SetZRotation(float z) {
        var transform1 = transform;
        var eulerAngles = transform1.eulerAngles;

        eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, z);
        transform1.eulerAngles = eulerAngles;
    }
}