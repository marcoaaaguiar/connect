﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class PieceLinker : MonoBehaviour {
    [SerializeField] private GameObject connectorPrefab;

    public static BasePiece SelectedPiece {
        get => Instance._selectedPiece;
        set {
            if (Instance._selectedPiece == null && value != null) {
                Instance.OnPieceClicked(value);
            }

            Instance._selectedPiece = value;
            Instance.UpdateLinks();
        }
    }

    public static BaseTile MouseOverTile {
        get => Instance._hoveredTile;
        set {
            Instance._hoveredTile = value;
            Instance.UpdateLinks();
        }
    }

    private static PieceLinker Instance { get; set; }
    [CanBeNull] private BasePiece _selectedPiece;
    [CanBeNull] private BaseTile _hoveredTile;
    private bool _canConnect;

    public static event EventHandler<List<BasePiece>> Connected;

    protected virtual void OnConnected(List<BasePiece> pieces) {
        Connected?.Invoke(this, pieces);
    }


    private void Awake() {
        if (Instance != null)
            Destroy(gameObject);
        else {
            Instance = this;
        }
    }

    private void Start() {
        _canConnect = true;
        WinCondition.Won += OnWon;
    }

    private void OnWon(object sender, EventArgs e) {
        _canConnect = false;
    }

    private void OnDestroy() {
        WinCondition.Won -= OnWon;
    }


    private void UpdateLinks() {
        if (!_canConnect || SelectedPiece == null || MouseOverTile == null ||
            !Board.Instance.AreNeighbors(SelectedPiece.tile, MouseOverTile)) return;

        // mouse is over a existing piece?
        if (MouseOverTile.piece != null) {
            // if hovering a piece of same group, connect it
            if (SelectedPiece.CanConnect(MouseOverTile.piece)) {
                MouseOverTile.piece.InvertLinks();
                Connect(SelectedPiece, MouseOverTile.piece);
                _selectedPiece = MouseOverTile.piece;
            }

            // Trying to return backstep the path?
            if (SelectedPiece.linkedFrom.Contains(MouseOverTile.piece)) {
                // If it is one of the initial pieces
                if (SelectedPiece.initial) {
                    Disconnect(MouseOverTile.piece, SelectedPiece);
                    _selectedPiece = MouseOverTile.piece;
                }
                else {
                    Destroy(SelectedPiece.gameObject);
                    _selectedPiece = MouseOverTile.piece;
                }
            }

            // override a tile (destroy a non initial piece of other group)
            if (MouseOverTile.piece.group != SelectedPiece.group &&
                !MouseOverTile.piece.initial) {
                Destroy(MouseOverTile.piece.gameObject);
                var piece = BasePiece.CreatePiece(connectorPrefab, MouseOverTile, SelectedPiece.group);
                if (SelectedPiece.CanConnect(piece)) {
                    Connect(SelectedPiece, piece);
                }

                _selectedPiece = piece;
            }
        }

        // if mouse is over an empty tile
        if (MouseOverTile.piece == null) {
            var piece = BasePiece.CreatePiece(connectorPrefab, MouseOverTile, SelectedPiece.group);
            if (SelectedPiece.CanConnect(piece)) {
                Connect(SelectedPiece, piece);
            }

            _selectedPiece = piece;
        }
    }

    private void OnPieceClicked(BasePiece piece) {
        if (!_canConnect || piece.linkedTo.Count <= 0) return;
        foreach (var linkedPiece in piece.linkedTo) {
            if (!linkedPiece.initial) {
                Destroy(linkedPiece.gameObject);
            }
            else {
                linkedPiece.linkedFrom.Remove(piece);
            }
        }

        piece.linkedTo.Clear();
    }

    private void Connect(BasePiece pieceFrom, BasePiece pieceTo) {
        pieceFrom.linkedTo.Add(pieceTo);
        pieceTo.linkedFrom.Add(pieceFrom);
        OnConnected(new List<BasePiece> {pieceFrom, pieceTo});
    }

    private static void Disconnect(BasePiece pieceFrom, BasePiece pieceTo) {
        pieceFrom.linkedTo.Remove(pieceTo);
        pieceTo.linkedFrom.Remove(pieceFrom);
    }
}