﻿using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

public abstract class BasePiece : MonoBehaviour {
    public BaseTile tile;
    public PieceGroup group;
    public bool initial;
    public ObservableCollection<BasePiece> linkedTo;
    public ObservableCollection<BasePiece> linkedFrom;
    [SerializeField] private int maxConnections;

    public static BasePiece CreatePiece(GameObject prefab, BaseTile tile, PieceGroup group, bool initial = false) {
        var pieceGameObject = Instantiate(prefab, tile.transform.position, Quaternion.identity);
        var piece = pieceGameObject.GetComponent<DefaultPiece>();

        piece.group = group;
        piece.initial = initial;

        // set piece color
        pieceGameObject.GetComponent<SpriteRenderer>().color = group.Color;

        // set piece to the tile
        tile.piece = piece;
        piece.tile = tile;

        // store variable
        group.Pieces.Add(piece);

        return piece;
    }

    private void Awake() {
        linkedTo = new ObservableCollection<BasePiece>();
        linkedFrom = new ObservableCollection<BasePiece>();
    }

    public bool IsConnected(BasePiece otherPiece) {
        return linkedFrom.Contains(otherPiece) || linkedTo.Contains(otherPiece);
    }

    public bool CanConnect(BasePiece otherPiece) {
        if (linkedFrom.Count + linkedTo.Count >= maxConnections ||
            otherPiece.linkedFrom.Count + otherPiece.linkedTo.Count >= otherPiece.maxConnections ||
            IsConnected(otherPiece) || otherPiece.@group != @group) return false;
        return true;
    }

    private void OnDestroy() {
        if (tile.piece == this) tile.piece = null;

        foreach (var piece in linkedFrom) {
            piece.linkedTo.Remove(this);
        }

        foreach (var piece in linkedTo) {
            piece.linkedFrom.Remove(this);
        }

        foreach (var piece in linkedTo) {
            if (!piece.initial) {
                Destroy(piece.gameObject);
            }
        }
    }

    public void InvertLinks() {
        var temp = linkedFrom.ToList();
        linkedFrom.Clear();
        foreach (var piece in linkedTo) {
            linkedFrom.Add(piece);
        }

        linkedTo.Clear();
        foreach (var piece in temp) {
            linkedTo.Add(piece);
            piece.InvertLinks();
        }
    }
}