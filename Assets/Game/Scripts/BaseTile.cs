﻿using UnityEngine;

public abstract class BaseTile : MonoBehaviour {
    public BasePiece piece;
    public abstract bool CanBeFilled { get; }
    
    
}