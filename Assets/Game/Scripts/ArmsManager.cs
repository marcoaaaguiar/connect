﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

public class ArmsManager : MonoBehaviour {
    private DefaultPiece _piece;
    [SerializeField] private List<Arms> arms;

    private void Start() {
        _piece = GetComponent<DefaultPiece>();
        arms = new List<Arms>();
        foreach (var arm in GetComponentsInChildren<Arms>()) {
            arms.Add(arm);
        }

        _piece.linkedTo.CollectionChanged += HandleLinkChanged;
        _piece.linkedFrom.CollectionChanged += HandleLinkChanged;
        SetCorrectArmsPosition();
    }

    private void HandleLinkChanged(object sender, NotifyCollectionChangedEventArgs e) {
        SetCorrectArmsPosition();
    }

    private void SetCorrectArmsPosition() {
        var pieces = new List<BasePiece>();
        pieces.AddRange(_piece.linkedFrom);
        pieces.AddRange(_piece.linkedTo);
        var armsLeft = arms.ToList();
        var relativeCoordinates = (from p in pieces select GetRelativeCoordinate(p.tile)).ToList();
        switch (relativeCoordinates.Count) {
            case 0:
                armsLeft.ForEach(arm => arm.SetArmPosition("rest"));
                break;
            case 1:
                if (relativeCoordinates[0] == "left" || relativeCoordinates[0] == "up") {
                    armsLeft[0].SetArmPosition(relativeCoordinates[0]);
                    armsLeft[1].SetArmPosition("rest");
                }
                else {
                    armsLeft[0].SetArmPosition("rest");
                    armsLeft[1].SetArmPosition(relativeCoordinates[0]);
                }

                break;
            case 2:
                if (relativeCoordinates.Contains("left")) {
                    arms[0].SetArmPosition("left");
                    armsLeft.Remove(arms[0]);
                    relativeCoordinates.Remove("left");
                }

                if (relativeCoordinates.Contains("right")) {
                    arms[1].SetArmPosition("right");
                    armsLeft.Remove(arms[1]);
                    relativeCoordinates.Remove("right");
                }

                for (var index = 0; index < relativeCoordinates.Count; index++) {
                    var coordinate = relativeCoordinates[index];
                    var arm = armsLeft[index];
                    arm.SetArmPosition(coordinate);
                }

                break;

            default:
                Debug.Log($"too many linked: {relativeCoordinates.Count}");
                break;
        }
    }

    private string GetRelativeCoordinate(BaseTile otherTile) {
        var thisCoordinate = Board.Instance.GetTileCoordinate(_piece.tile);
        var otherTileCoordinate = Board.Instance.GetTileCoordinate(otherTile);

        if (thisCoordinate.HasValue && otherTileCoordinate.HasValue) {
            if (thisCoordinate.Value.Item1 + 1 == otherTileCoordinate.Value.Item1)
                return "right";
            if (thisCoordinate.Value.Item2 + 1 == otherTileCoordinate.Value.Item2)
                return "up";
            if (thisCoordinate.Value.Item1 - 1 == otherTileCoordinate.Value.Item1)
                return "left";
            if (thisCoordinate.Value.Item2 - 1 == otherTileCoordinate.Value.Item2)
                return "down";
        }

        return "";
    }
}