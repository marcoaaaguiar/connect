﻿using System;
using System.Collections.Generic;

[Serializable]
public class BoardData {
    public int sizeX;
    public int sizeY;
    public string text;
    public List<string> tiles;
    
}