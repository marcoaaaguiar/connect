﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BoardCreator : MonoBehaviour {
    public static BoardCreator Instance;

    private LevelData LevelData { get; set; }

    [SerializeField] private GameObject defaultPiece;
    [SerializeField] private GameObject defaultTile;

    public event EventHandler BoardCreated;
    public bool BoardHasBeenCreated { get; private set; }

    private void Awake() {
        if (Instance != null)
            Destroy(gameObject);
        else {
            Instance = this;
        }

        BoardHasBeenCreated = false;
    }


    private void Start() {
        LevelData = LevelLoader.Instance.GetCurrentLevelData();

        Board.Instance.Tiles = new BaseTile[LevelData.board.sizeX, LevelData.board.sizeY];
        Board.Instance.Pieces = new List<BasePiece>();
        Board.Instance.Groups = new List<PieceGroup>();

        CreateTiles();
        CreateGroups();
        CreateInitialPiece();
        CreatePaths();

        OnBoardCreated();
    }

    private static void CreatePaths() {
        foreach (var group in Board.Instance.Groups) {
            if (group.Pieces.Count == 2) {
                var path = new PiecePath {
                    Pieces = new List<BasePiece>(@group.Pieces.ToArray()),
                    Start = @group.Pieces[0],
                    End = @group.Pieces[1]
                };
                group.Paths.Add(path);
            }
        }
    }

    private void CreateTiles() {
        for (var xIndex = 0; xIndex < LevelData.board.sizeX; xIndex++) {
            for (var yIndex = 0; yIndex < LevelData.board.sizeY; yIndex++) {
                var offset = new Vector2(0.5f, 0.5f);
                var position = new Vector2(xIndex - (float) LevelData.board.sizeX / 2, yIndex - (float) LevelData.board.sizeY / 2);
                var tile = Instantiate(defaultTile, position + offset, Quaternion.identity, transform);
                tile.name = "(" + xIndex + "," + yIndex + ")";
                Board.Instance.Tiles[xIndex, yIndex] = tile.GetComponent<BaseTile>();
            }
        }
    }

    private void CreateGroups() {
        for (var gIndex = 0; gIndex < LevelData.groups; gIndex++) {
            var group = new PieceGroup();
            group.GroupId = gIndex;
            group.Color = Color.HSVToRGB((float) gIndex / LevelData.groups, 0.6f, 1.0f);
            Board.Instance.Groups.Add(group);
        }
    }

    private void CreateInitialPiece() {
        for (var index = 0; index < LevelData.points.Count; index++) {
            var pieceData = LevelData.points[index];
            var piece = BasePiece.CreatePiece(defaultPiece,
                Board.Instance.Tiles[pieceData.position[0], pieceData.position[1]],
                Board.Instance.Groups[pieceData.group], true);
            Board.Instance.Pieces.Add(piece);
            piece.gameObject.name = "piece-group-" + pieceData.group + "-number" + index;
        }
    }

    protected virtual void OnBoardCreated() {
        BoardHasBeenCreated = true;
        BoardCreated?.Invoke(this, EventArgs.Empty);
    }
}