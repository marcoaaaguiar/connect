﻿using System.Linq;
using UnityEngine;

public class PathsChecker : MonoBehaviour {
    // Start is called before the first frame update
    public static int CountConnectedPaths() {
        var counter = 0;
        foreach (var group in Board.Instance.Groups) {
            foreach (var path in group.Paths) {
                counter += CheckPath(path.Start, path.End) ||
                           CheckPath(path.End, path.Start)
                    ? 1
                    : 0;
            }
        }

        return counter;
    }

    private static bool CheckPath(BasePiece piece, BasePiece endPiece) {
        if (piece.linkedTo.Count == 0) return false;
        return piece.linkedTo.Contains(endPiece) || piece.linkedTo.All(p => CheckPath(p, endPiece));
    }
}