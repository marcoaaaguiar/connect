﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ConnectedPathsText : MonoBehaviour {
    [SerializeField] private string text;

    private int _totalGroups;
    private TextMeshProUGUI _textMeshProUgui;

    private void Awake() {
        _textMeshProUgui = GetComponent<TextMeshProUGUI>();
    }

    private void Start() {
        // subscribe to the piece linker
        PieceLinker.Connected += OnConnected;
        // if BoardCreator already created do startup else subscribe 
        if (BoardCreator.Instance.BoardHasBeenCreated) {
            StartUpOperations();
        }
        else {
            BoardCreator.Instance.BoardCreated += OnBoardCreated;
        }
    }

    private void OnBoardCreated(object obj, EventArgs args) {
        StartUpOperations();
    }

    private void StartUpOperations() {
        _totalGroups = Board.Instance.Groups.Count;
        UpdateText();
    }

    private void OnConnected(object sender, List<BasePiece> e) {
        UpdateText();
    }

    private void UpdateText() {
        var pathsConnected = PathsChecker.CountConnectedPaths();
        _textMeshProUgui.text = string.Format(text, pathsConnected, _totalGroups);
    }
}