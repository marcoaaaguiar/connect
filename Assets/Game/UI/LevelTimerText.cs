﻿using TMPro;
using UnityEngine;

public class LevelTimerText : MonoBehaviour
{
    [SerializeField] private string text;
    private TextMeshProUGUI _textMeshProUgui;

    private void Awake() {
        _textMeshProUgui = GetComponent<TextMeshProUGUI>();
    }

    private void Update() {
        _textMeshProUgui.text = string.Format(text, LevelTimer.Instance);
    }
}
