﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class SaveData {
    public DateTime dateTime;

    public Dictionary<string, bool> locked = new Dictionary<string, bool>();
    public Dictionary<string, bool> completed = new Dictionary<string, bool>();

    [NonSerialized] public string FilePath;

    public SaveData Copy() {
        return JsonConvert.DeserializeObject<SaveData>(JsonConvert.SerializeObject(this));
    }
    
  
}