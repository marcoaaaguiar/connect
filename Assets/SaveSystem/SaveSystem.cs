﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

public class SaveSystem : MonoBehaviour {
    public static SaveSystem Instance;

    private const int MaxSaveFiles = 10;
    private static List<SaveData> _saveData;
    private static string _savesFolderPath;

    public static SaveData Current;

    private void Awake() {
        // Object Persistence
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        // Initialize object
        _savesFolderPath = Path.Combine(Application.persistentDataPath, "save");
        if (!Directory.Exists(_savesFolderPath)) {
            Directory.CreateDirectory(_savesFolderPath);
        }

        _saveData = LoadSaveData();

        // if no save loaded create a new save
        Current = _saveData.Count > 0 ? _saveData.Last().Copy() : new SaveData();
        
        // Remove Excess files
        DeleteExcessFiles();
    }

    private static List<SaveData> LoadSaveData() {
        var allSaves = new List<SaveData>();

        foreach (var file in Directory.GetFiles(_savesFolderPath)) {
            if (Path.GetFileName(file).StartsWith("save") && Path.GetFileName(file).EndsWith(".json")) {
                var text = File.ReadAllText(file);
                try {
                    var save = JsonConvert.DeserializeObject<SaveData>(text);
                    save.FilePath = file;
                    allSaves.Add(save);
                }
                catch (ArgumentException e) {
                    Debug.Log($"Failed to load save file: {file} | Exception: {e} | Data: {text}");
                }
            }
        }

        allSaves = allSaves.OrderBy(data => data.dateTime).ToList();
        return allSaves;
    }

    public static void Save() {
        Current.dateTime = DateTime.Now;

        var savePath = Path.Combine(_savesFolderPath, $"save_{Current.dateTime:yyyy-dd-M--HH-mm-ss}.json");
        File.WriteAllText(savePath, JsonConvert.SerializeObject(Current));
        
        Current = Current.Copy();
        DeleteExcessFiles();
    }

    private static void DeleteExcessFiles() {
        if (_saveData.Count <= MaxSaveFiles) return;
        for (var i = 0; i < _saveData.Count - MaxSaveFiles; i++) {
            File.Delete(_saveData[i].FilePath);
        }
            
        _saveData.RemoveRange(0, _saveData.Count - MaxSaveFiles);
    }
    
}